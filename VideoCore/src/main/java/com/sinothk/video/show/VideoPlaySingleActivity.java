package com.sinothk.video.show;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kk.taurus.playerbase.entity.DataSource;
import com.sinothk.video.core.R;
import com.sinothk.video.core.VideoPlayerActivity;
import com.sinothk.video.core.bean.VideoSourceEntity;

public class VideoPlaySingleActivity extends VideoPlayerActivity {

    DataSource dataSource = new DataSource();
    VideoSourceEntity videoSource;

    LinearLayout titleBarView;
    TextView titleTxtTv;

    public static void start(Activity mActivity, VideoSourceEntity videoSource) {
        Intent intent = new Intent(mActivity, VideoPlaySingleActivity.class);
//        DataSource dataSource = new DataSource(DataUtils.VIDEO_URL_08);//DataUtils.VIDEO_URL_08
//        dataSource.setTitle("音乐和艺术如何改变世界");

        Bundle bundle = new Bundle();
        bundle.putSerializable("DataSource", videoSource);
        bundle.putString("titleTxt", videoSource.getTitle());
        intent.putExtras(bundle);

        mActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        videoSource = (VideoSourceEntity) getIntent().getSerializableExtra("DataSource");
        super.onCreate(savedInstanceState);

        titleTxtTv = findViewById(R.id.titleTxtTv);
        titleBarView = findViewById(R.id.titleBarView);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

       String titleTxt = getIntent().getStringExtra("titleTxt");
        titleTxtTv.setText(titleTxt);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_video_play_single;
    }

    @Override
    public DataSource getDataSource() {
        if (videoSource == null || TextUtils.isEmpty(videoSource.getTitle()) || TextUtils.isEmpty(videoSource.getUrl())) {
            finish();
            Toast.makeText(this, "标题、地址不能为空", Toast.LENGTH_SHORT).show();
            return null;
        }

        dataSource.setTitle(videoSource.getTitle());
        dataSource.setData(videoSource.getUrl());
        return dataSource;
    }

    @Override
    protected void hideBottomUIMenu2() {
        super.hideBottomUIMenu2();
        if (titleBarView != null) {
            titleBarView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void showBottomUIMenu2() {
        super.showBottomUIMenu2();
        if (titleBarView != null) {
            titleBarView.setVisibility(View.VISIBLE);
        }
    }
}
